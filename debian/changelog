fcml (1.1.3-4) UNRELEASED; urgency=medium

  * d/watch: Use https protocol

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:58:34 +0200

fcml (1.1.3-3) unstable; urgency=medium

  * Move libfcml0 to libs.
  * Migrate to Salsa.
  * Switch to debhelper compatibility level 11.
  * Enable all hardening options.
  * Update debian/copyright.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 02 May 2018 20:13:35 +0200

fcml (1.1.3-2) unstable; urgency=medium

  * Fix debian/watch so it doesn't match subdirectories.
  * Mark libfcml-dev Multi-Arch: same (thanks, Multiarch hinter!).
  * Switch to debhelper compatibility level 10.

 -- Stephen Kitt <skitt@debian.org>  Sun, 06 Nov 2016 20:00:15 +0100

fcml (1.1.3-1) unstable; urgency=medium

  * New upstream release, fixing the tests. Closes: #800937, #841191.
  * Switch to https: VCS URIs (see #810378).
  * Mark libfcml-doc Multi-Arch: foreign.
  * Clean up debian/control using cme.
  * Standards-Version 3.9.8, no change required.
  * Add support for openjdk-9-jre-headless (and enhance the -headless
    variants of openjdk-7-jre and openjdk-8-jre).

 -- Stephen Kitt <skitt@debian.org>  Tue, 18 Oct 2016 22:21:47 +0200

fcml (1.1.1-2) unstable; urgency=medium

  * Link the internal tests statically (otherwise they fail on s390x).

 -- Stephen Kitt <skitt@debian.org>  Tue, 08 Sep 2015 21:02:18 +0200

fcml (1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Use unique license names in debian/copyright.

 -- Stephen Kitt <skitt@debian.org>  Tue, 11 Aug 2015 23:26:16 +0200

fcml (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Install newly-provided pkgconfig file.

 -- Stephen Kitt <skitt@debian.org>  Tue, 03 Feb 2015 23:55:05 +0100

fcml (1.0.0-1) unstable; urgency=low

  * Initial release. Closes: #775872.

 -- Stephen Kitt <skitt@debian.org>  Tue, 03 Feb 2015 00:11:56 +0100
